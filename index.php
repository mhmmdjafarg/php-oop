<?php
    require('animal.php');
    require('frog.php');
    require('ape.php');

    $sheep = new animal("sheep");
    echo "Animal 1 <br>";
    echo "Name of the animal : $sheep->name <br>";
    echo "Total legs : $sheep->legs <br>";
    echo "Cold blooded ? ";
    echo boolval($sheep->cold_blooded) ? 'true' : 'false';
    echo "<br><br>";


    $frog = new frog("Berudu");
    echo "Animal 2<br>";
    echo "Name of the animal : $frog->name <br>";
    echo "Total legs : $frog->legs <br>";
    echo "Cold blooded ? ";
    echo boolval($frog->cold_blooded) ? 'true' : 'false';
    echo "<br>";
    $frog->jump();
    echo "<br><br>";


    $ape = new ape("Kera sakti");
    echo "Hewan 3<br>";
    echo "Name of the animal : $ape->name <br>";
    echo "Total legs : $ape->legs <br>";
    echo "Cold blooded ? ";
    echo boolval($ape->cold_blooded) ? 'true' : 'false';
    echo "<br>";
    $ape->yell();
    echo "<br><br>";
?>